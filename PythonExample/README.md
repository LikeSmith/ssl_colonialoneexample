# Python Example

This page provides an explanation of the Python example.  The example code trains an AutoEncoder on the MNIST data set (a series of hand written numerals).  This AutoEncoder is based on a keras [tutorial](https://blog.keras.io/building-autoencoders-in-keras.html) on the subject.

## What's in the example?

The first thing to do is look in the MatlabExample directory.  This directory should have the following files:

+ PythonExample.py
+ PythonExample_plotResults.py
+ runScript.sh

The PythonExample.py file is the script we are going to run on the cluster.  PythonExample_plotResults.py is a script for ploting the results. The runTrainer.sh is a shell script that will be run by the node on the cluster.

## runScript.sh

The runScript.sh file is the most important to us for running the example code.  The first bit of the file are a series of comments that tell slurm how to run the node.  The first set of commands tell slurm where to dump the output and error streams.  We dump them to a text file for review later.

```
#SBATCH -o PythonExample%j.out
#SBATCH -e PythonExample%j.err
```

the `%j` is a placeholder term for the job number.  This assures that the outputs of other jobs will not be overwritten.

Next, tell slurm to email you when your job starts, and when it is completed.  Do not forget to replace the placeholder with your email.

```
#SBATCH --mail-type=ALL
#SBATCH --mail-user=<your email here>
```

We need to tell slurm what queue to put our job in.  There are several different partitions, see the [Colonial One wiki](https://colonialone.gwu.edu/) for more information.  We tell slurm that we want to use 1 node on the debug partition.

```
#SBATCH -N 1
#SBATCH -p debug
```

Now, we need to set the current directory.  This will vary depending on where you cloned the repo to, we assume you cloned it to your home directory.  You will need to add your username in place of the placeholder and adjust the path as is necessary.

```
#SBATCH -D /home/<username>/ssl_colonialoneexample/PythonExample
```

The last bit of the set up is to name the job and specify a time limit.  We just name the job MatlabExample, and give it a 2 hour time limit.

```
#SBATCH -J PythonExample
#SBATCH -t 02:00:00
```

Now that we have the node set up, we just need to list the commands necessary to run the desired script.  The first thing we need to do is load the modules necessary for executing our script.  For this example, we need to load the anaconda 4.2.0 module.

```
module load anaconda/4.2.0
```

Lastly all we need to do is run the python script.

```
python PythonExample.py
```

Special thanks to Shankar Kulumani for providing the template for this script.

## Starting the Job

Now that we have the script file, all we need to do is pass it to slurm and tell it to schedule a job with it.  This is done with the following command:

```
$ sbatch runScript.sh
```

This will schedule a job with the specifications listed in the script.  The job may not start immediately depending on how busy Colonial One is when you schedule the job.  You will get an email at the specified address when the job starts, as well as when it ends.  You can check on the status of the job using the command `squeue -l`. Once the job is complete, you should see two text files that contain the output of the example, as well as a .pickle file containing trained autoencoder as well as the training data and the results from being run through the autoencoder.  To view the results, you must first commit these files to the repo and push the changes to your own (We suggest forking this repo to your own account and cloning it from there).  Then clone the repo and run the PythonExample_plotResults.py script on another machine with a graphical interface.  Alternatively, the PythonExample_plotResults.py script should work in a login shell if you allow for X11 forwarding in you ssh connection.
