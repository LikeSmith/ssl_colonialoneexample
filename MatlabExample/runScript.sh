#!/bin/bash

# set output and error output filenames, %j will be replaced by Slurm with the jobid
#SBATCH -o MatlabExample_%j.out
#SBATCH -e MatlabExample_%j.err 

# email me when job starts and stops
#SBATCH --mail-type=ALL
#SBATCH --mail-user=<your email here>

# set up which queue should be joined
#SBATCH -N 1
#SBATCH -p debug

# set the correct directory
#SBATCH -D /home/<username>/ssl_colonialoneexample/MatlabExample

# name job
#SBATCH -J MatlabExample

# set time limit
#SBATCH -t 02:00:00

# load necesary modules
module load matlab

# run task
matlab -nodesktop < MatlabExample.m

