## [GWU Colonial One](https://colonialone.gwu.edu/) Tutorial

This repo is meant as an example for running projects on the Colonial One computing cluster at the George Washington University. The examples presented in this example show how to train autoencoders designed in both Matlab and Python3 (using the keras library) since the Smart Systems Lab primarily uses Colonial One to train Neural Networks.

## Logging into Colonial one

You need to have a terminal emulator. 
This alredy exists on MacOS/Linux but you can install [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) on Windows.

The first thing to do is to log into a login shell in Colonial One. To do this on a Linux machine, make sure you have an ssh client installed, then run

~~~
ssh <username>@login.colonialone.gwu.edu
~~~

where `<username>` is your GWU Net ID. 
The part of your email before the `@`.

### Verifying SSH Fingerprint

On your first connection to the HPC, you'll be prompted to accept the ssh keys and verify the fingerprint of ColonialOne.
The [main page](https://colonialone.gwu.edu/) for ColonialOne still lists the older `md5` hash of the fingerprint. 
The new standard is the use `sha256`, you can easily switch between them by using the commands below:

~~~
$ ssh -o FingerprintHash=sha256 <username>@login.colonialone.gwu.edu
The authenticity of host 'login.colonialone.gwu.edu (161.253.199.5)' can't be established.
RSA key fingerprint is MD5:77:39:f1:fb:e4:6d:f4:38:bb:c6:ba:08:0e:b4:b8:e3.
Are you sure you want to continue connecting (yes/no)? 
~~~

~~~
$ ssh -o FingerprintHash=md5 <username>@login.colonialone.gwu.edu
The authenticity of host 'login.colonialone.gwu.edu (161.253.199.5)' can't be established.
RSA key fingerprint is SHA256:tW7cHhYKl7J7YjWhCv3Wqn4ozfO4cTmEGWlvbU154To.
Are you sure you want to continue connecting (yes/no)? 
~~~

You want to make sure that the fingerprint matches with those given or you're connecting to a compromised ColonialOne.

## Clone the repo

Once this is done, you will need to clone this repo. Navigate to where you want to download this repo, then you will need to load the git module, and clone the repo with the following commands:

~~~
moudle load git
git clone https://bitbucket.org/LikeSmith/ssl_colonialoneexample.git
~~~

You have to examples given in the directories:

1. `./MatlabExample`
2. `./PythonExample`

