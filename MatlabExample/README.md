# Matlab Example

This page provides an explanation of the matlab example.  The example code trains an AutoEncoder on the MNIST data set (a series of hand written numerals).  This AutoEncoder is based on the AutoEncoder class that is built using the Matlab Neural Network toolbox, and consists of 3 layers.

## What's in the example?

The first thing to do is look in the MatlabExample directory.  This directory should have the following files:

+ loadMNIST.m
+ MatlabExample.m
+ runScript.sh
+ MNIST (directory)

The first m file is a function for loading the MNIST dataset (stored in the MNIST directory).  The MatlabExample.m file is the script we are going to run on the cluster. The runTrainer.sh is a shell script that will be run by the node on the cluster.

## runScript.sh

The runScript.sh file is the most important to us for running the example code.  The first bit of the file are a series of comments that tell slurm how to run the node.  The first set of commands tell slurm where to dump the output and error streams.  We dump them to a text file for review later.

```
#SBATCH -o MatlabExample%j.out
#SBATCH -e MatlabExample%j.err
```

the `%j` is a placeholder term for the job number.  This assures that the outputs of other jobs will not be overwritten.

Next, tell slurm to email you when your job starts, and when it is completed.  Do not forget to replace the placeholder with your email.

```
#SBATCH --mail-type=ALL
#SBATCH --mail-user=<your email here>
```

We need to tell slurm what queue to put our job in.  There are several different partitions, see the [Colonial One wiki](https://colonialone.gwu.edu/) for more information.  We tell slurm that we want to use 1 node on the debug partition.

```
#SBATCH -N 1
#SBATCH -p debug
```

Now, we need to set the current directory.  This will vary depending on where you cloned the repo to, we assume you cloned it to your home directory.  If you did not, change this value accordingly.

```
#SBATCH -D ~/ssl_colonialoneexample/MatlabExample
```

The last bit of the set up is to name the job and specify a time limit.  We just name the job MatlabExample, and give it a 2 hour time limit.

```
#SBATCH -J MatlabExample
#SBATCH -t 02:00:00
```

Now that we have the node set up, we just need to list the commands necessary to run the desired script.  The first thing we need to do is load the modules necessary for executing our script.  For this example, all we need is the matlab module.

```
module load matlab
```

Lastly all we need to do is call matlab and tell it to execute our script.

```
matlab -nodesktop < MatlabExample.m
```

Note that we use the `-nodesktop` flag so matlab does not try to launch its gui.

Special thanks to Shankar Kulumani for providing the template for this script.

## Starting the Job

Now that we have the script file, all we need to do is pass it to slurm and tell it to schedule a job with it.  This is done with the following command:

```
$ sbatch runScript.sh
```

This will schedule a job with the specifications listed in the script.  The job may not start immediately depending on how busy Colonial One is when you schedule the job.  You will get an email at the specified address when the job starts, as well as when it ends.  You can check on the status of the job using the command `squeue -l`. Once the job is complete, you should see two text files that contain the output of the example, as well as a .mat file containing the workspace when the matlab script finished executing.  Note that to view the results, you can access them through the login shell (we recommend using X11 forwarding in the ssh connection to do this) or if you forked the repo to begin with, you can just push the changes to your fork, then clone it onto another computer.

