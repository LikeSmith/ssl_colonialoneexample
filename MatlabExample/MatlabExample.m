clear all
close all

% params
n = 784; % number of inputs
n_h = 100; % number of nodes in hidden layer

% setup network
net = network;

net.numInputs = 1;
net.numLayers = 2;

net.biasConnect = [1; 1];
net.inputConnect = [1; 0];
net.layerConnect = [0 0; 1 0];
net.outputConnect = [0 1];

net.inputWeights{1, 1}.initFcn = 'randsmall';
net.layerWeights{2, 1}.initFcn = 'randsmall';

net.biases{1}.initFcn = 'randsmall';
net.biases{2}.initFcn = 'randsmall';

net.inputs{1}.size = n;

net.layers{1}.size = n_h;
net.layers{1}.transferFcn = 'logsig';
net.layers{1}.initFcn = 'initwb';

net.layers{2}.size = n;
net.layers{2}.transferFcn = 'logsig';
net.layers{2}.initFcn = 'initwb';

net.initFcn = 'initlay';
net.performFcn = 'msesparse';
net.trainFcn = 'trainscg';
net.divideFcn = 'dividetrain';
net.plotFcns = {'plotperform', 'plottrainstate'};
net.trainParam.showCommandLine = true;
net.performParam.L2WeightRegularization = 0.004;
net.performParam.sparsityRegularization = 4;
net.performParam.sparsity = 0.15;

net = init(net);

% load MNIST data
[tst_i, tst_l, trn_i, trn_l] = loadMNIST('MNIST');

dat = zeros(784, 70000);

for i = 1:10000
    for j = 1:28
        for k = 1:28
            dat((j-1)*28 + k, i) = tst_i(j, k, i);
        end
    end
end

for i = 1:60000
    for j = 1:28
        for k = 1:28
            dat((j-1)*28 + k, 10000+i) = trn_i(j, k, i);
        end
    end
end

clear tst_i tst_l trn_i trn_l

net = train(net, dat, dat);

save net

